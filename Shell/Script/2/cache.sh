#!/bin/bash

# Function to purge caches
purging () {
    sudo sync
    sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 2 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 2 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 2 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'
    sudo sync
    sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'
}

# Function to generate the report
report() {
    local ip_address=$(curl -s -4 ident.me)
    echo -e "\n\n\n\nServer ${ip_address}'s cache Purging report:-\n-----Before Purging-----\n$(cat ~/old)\n\n-----After Purging-----\n$(free -wh)\nTimeStamp:- $(TZ='Asia/Kolkata' date)\n\n"
}

# Record the memory usage before purging
free -wh > ~/old

# Purge caches and generate the report
purging > /dev/null 2>&1 && report || echo -e "Kindly run with sudo rights\nExample:- sudo $0"
