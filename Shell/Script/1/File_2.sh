#!/bin/bash

file_name="file_1.txt"
file_path="/home/a1j2ithkumar/"                                 # path for "file_1.txt"
files_list=$(find "$file_path" -name "$file_name")

for file in $file_name
do
    if [ -e $file ]
    then
        sed -i 's/Apples/Oranges/g' $file
    fi
done

# it save the output in a file_1.txt after run the script.
