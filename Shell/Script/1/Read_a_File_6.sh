#!/bin/bash

# Read the text file line by line
while IFS='|' read -r name age profession; do
    echo "Name: $name"
    echo "Age: $age"
    echo "Profession: $profession"
    echo "---"
done < Read_a_File_6.txt


# MultipleLine Comments
: '

$ ./Read_a_File_6.sh
Name: John Doe
Age: 25
Profession: Engineer
---
Name: Jane Smith
Age: 30
Profession: Designer
---
Name: Michael Johnson
Age: 28
Profession: Manager
---

'
