#!/bin/bash

# Read the file line by line
while read -r username; do
    # Use the 'adduser' command to create a new user
    sudo adduser --quiet --disabled-password --gecos "" $username

    # Print a message to the console
    echo "Created user $username."
done < users.txt

# Output:
# Created user john.
# Created user jane.
# Created user mike.
