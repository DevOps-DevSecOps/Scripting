#!/bin/bash

# Running a command in the background
sleep 5 &
echo "Background process started."

# Wait for the background process to finish
wait
echo "Background process completed."


# MultipleLine Comments
: '

$ ./echo_5.sh
Background process started.
[1]+ Done sleep 5
Background process completed.

'
