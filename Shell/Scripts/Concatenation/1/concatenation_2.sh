#!/bin/bash

string1="Hello, "
string2="World!"
result=$string1$string2
echo "Concatenated string: $result"


# MultipleLine Comments
: '

$ ./concatenation_2.sh
Concatenated string: Hello, World!

'
