# Declare a global variable
global_var="I'm global"

# Declare a function with local variables
local_variables() {
    local local_var="I'm local"
    echo "Inside function: $local_var"
    echo "Inside function: $global_var"
}

# Call the local_variables function
local_variables

# Access global variable outside the function
echo "Outside function: $global_var"
# Attempting to access local_var here will result in an error
