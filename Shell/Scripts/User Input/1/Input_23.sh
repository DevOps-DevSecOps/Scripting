#!/bin/bash
# Read password without echoing characters
read -s -p "Enter your password: " password
echo "Password entered."
# Read input with timeout
read -t 5 -p "Enter something in 5 seconds: " timed_input
echo "You entered: $timed_input"



# MultipleLine Comments
: '
-p specifies a prompt message.
-r disables interpreting backslashes, useful for reading file paths.
-t sets a timeout for input.
-s hides input (useful for passwords).
'
