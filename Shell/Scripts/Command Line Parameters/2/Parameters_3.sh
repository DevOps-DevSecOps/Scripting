#!/bin/bash

echo "Script name: $0"
echo "First argument: $1"
echo "Second argument: $2"
echo "Total number of arguments: $#"
echo "All arguments as list: $@"
echo "All arguments as string: $*"


# MultipleLine Comments
: '

$ chmod +x Parameters_3.sh
$ ./Parameters_3.sh arg1 arg2 arg3
Script name: ./args_script.sh
First argument: arg1
Second argument: arg2
Total number of arguments: 3
All arguments as list: arg1 arg2 arg3
All arguments as string: arg1 arg2 arg3

'
