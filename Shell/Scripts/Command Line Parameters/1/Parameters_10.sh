#!/bin/bash

for s in $@
do
    echo "Entered ARG is $s"
    echo "Enter the count of ARG is $#"
done



# MultipleLine Comments
: '

$ bash Parameters_10.sh DevOps DevSecOps GitOps
Entered ARG is DevOps
Enter the count of ARG is 3
Entered ARG is DevSecOps
Enter the count of ARG is 3
Entered ARG is GitOps
Enter the count of ARG is 3

'
