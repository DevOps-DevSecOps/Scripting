#!/bin/bash
# Program name: "command_line_add.sh"
# shell script program to add two numbers using command line arguments.
sum=`expr $1 + $2`
echo "Sum is: $sum"
