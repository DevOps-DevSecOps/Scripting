#!/bin/bash
fruits=("Apple" "Banana" "Orange")

# Loop through array using for loop
echo "Using for loop:"
for fruit in "${fruits[@]}"; do
    echo "Fruit: $fruit"
done

# Loop through array using while loop and index
echo "Using while loop:"
index=0
while [ $index -lt ${#fruits[@]} ]; do
    echo "Fruit at index $index: ${fruits[$index]}"
    index=$((index + 1))
done
