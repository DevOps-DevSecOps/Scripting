#!/bin/bash

# Declare an array with values
fruits=("Apple" "Banana" "Orange")

# Access array elements
echo "First fruit: ${fruits[0]}"
echo "Second fruit: ${fruits[1]}"
echo "Third fruit: ${fruits[2]}"
