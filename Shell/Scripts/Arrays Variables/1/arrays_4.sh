#!/bin/bash

fruits=("Apple" "Banana" "Orange")

# Adding an element
fruits+=("Grapes")

# Updating an element
fruits[1]="Mango"

# Removing an element
unset fruits[0]

# Display the modified array
echo "Modified array:"
for fruit in "${fruits[@]}"; do
    echo "Fruit: $fruit"
done
