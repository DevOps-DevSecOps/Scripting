#!/bin/bash

string="Hello, World!"
substring=${string:7:5} # Starting from index 7, extract 5 characters

echo "Substring: $substring"


# MultipleLine Comments
: '

$ ./extract_2.sh
Substring: World

'
