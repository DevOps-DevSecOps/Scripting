# String Variable
name="John"
echo "Hello, $name!" # Output: Hello, John!

# Integer Variable
age=25
echo "Age: $age years" # Output: Age: 25 years
