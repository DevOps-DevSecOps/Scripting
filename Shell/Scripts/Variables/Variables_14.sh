#!/bin/bash

name="John"
age=25

echo "Debug: Starting script..."
echo "Debug: Name is $name"
echo "Debug: Age is $age"

result=$((age * 2))
echo "Debug: Result is $result"

echo "Debug: Script completed."



# MultipleLine Comments
: '

$ ./Variables_14.sh
Debug: Starting script...
Debug: Name is John
Debug: Age is 25
Debug: Result is 50
Debug: Script completed.

'
