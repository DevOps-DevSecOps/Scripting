import sys

type = sys.argv[1]

if type == "t2.micro":
    print("charge 2 dollars a day")
elif type == "t2.medium":
    print("charge 4 dollars a day")
elif type == "t2.large":
    print("charge 8 dollars a day")
else:
    print("please provide a valid instance type")
